Solution Details
--
Built with .Net framework 4.7.2
C#, ASP.NET WEB API
Swagger UI for API documentation


Cloning and Running
--
git clone https://dfordeley@bitbucket.org/dfordeley/beer.git
Solution can be open and built with visual studio
The solution displays swagger Ui which makes it easier for tests
Database.Json stores a list of request 


Code Structure
--
BeerController - Contains the API endpoints required
Model Folder - Contain classes for Json required for API requests and response
Utils - Contains reusable static codes
Tests Project - Unit tests for the solution
