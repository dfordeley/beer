﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BeerChallenge.Helpers;
using BeerChallenge.Helpers.Filter;
using BeerChallenge.Models;
using Newtonsoft.Json;

namespace BeerChallenge.Controllers
{
    /// <summary>
    /// </summary>
    [RoutePrefix("api/beer")]
    public class BeerController : ApiController
    {
        /// <summary>
        /// Punk API url retrieved from the config
        /// </summary>
        private readonly string _host = ConfigurationManager.AppSettings["Host"];

        /// <summary>
        /// Add rating to the specified beer Id
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [Validation]
        [Route("AddRating")]
        [HttpPost]
        public async Task<IHttpActionResult> AddRating(int id, RatingDetails request)
        {
            if (request != null && (request.Rating > 5 || request.Rating < 1))
                return BadRequest("Invalid rating. Rating should be between 1 & 5");

            var (isSuccessful, data) = await ExternalApiCall($"beers/{id}");

            if (isSuccessful)
            {
                Utils.UpdateJson(request);

                return Ok("Rating Added Successfully!");
            }

            var apiResponse = JsonConvert.DeserializeObject<FailedResponse>(data);
            return BadRequest($"Error:{apiResponse?.Message}");
        }

        /// <summary>
        /// Get All beers matching the query name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Route("GetBeers")]
        [HttpGet]
        public async Task<IHttpActionResult> GetBeers(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return BadRequest("Please enter a valid name");

            var constructedName = Utils.PunkApiBeerNameSearchFormat(name);

            var (isSuccessful, data) = await ExternalApiCall($"beers?beer_name={constructedName}");

            if (!isSuccessful)
                return NotFound();
            
            var result = Utils.GenerateGetBeerResponse(data);
            return Ok(result);
        }

        /// <summary>
        /// Function to call external Apis
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private async Task<Tuple<bool, string>> ExternalApiCall(string query)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync($"{_host}{query}"))
                {
                    var status = response.IsSuccessStatusCode;
                    var data = await response.Content.ReadAsStringAsync();

                    return new Tuple<bool, string>(status, data);
                }
            }
        }
    }
}