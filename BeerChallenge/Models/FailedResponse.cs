﻿using Newtonsoft.Json;

namespace BeerChallenge.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FailedResponse
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("statusCode")]
        public string StatusCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}