﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BeerChallenge.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BeerDetails
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("userRatings")]
        public List<RatingDetails> UserRatings { get; set; }
    }

}