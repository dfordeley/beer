﻿using Newtonsoft.Json;

namespace BeerChallenge.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RatingDetails
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("rating")]
        public int Rating { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("comments")]
        public string Comments { get; set; }
    }
}