using System.Web.Http;
using WebActivatorEx;
using BeerChallenge;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace BeerChallenge
{
    /// <summary>
    /// 
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        /// 
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "BeerChallenge");
                c.IncludeXmlComments(string.Format(@"{0}\bin\BeerChallenge.xml", System.AppDomain.CurrentDomain.BaseDirectory));
            }).EnableSwaggerUi();
        }
    }
}
