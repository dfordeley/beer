﻿using BeerChallenge.Models;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BeerChallenge.Helpers.Filter
{
    /// <summary>
    /// 
    /// </summary>
    public class ValidationAttribute : ActionFilterAttribute
    {
        readonly string _parameter = "request";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ActionArguments.ContainsKey(_parameter))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized));

            if (actionContext.ActionArguments[_parameter] is RatingDetails requestData && !EmailRegexValidation(requestData.Username))
            {
                actionContext.Response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent("Invalid Email Supplied")
                };
            }
            base.OnActionExecuting(actionContext);
        }
    

        private static bool EmailRegexValidation(string email)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            var match = regex.Match(email);
            return match.Success;
        }
    }
}