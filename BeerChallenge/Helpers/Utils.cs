﻿using BeerChallenge.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace BeerChallenge.Helpers
{
    /// <summary>
    /// Utility Class
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Get current domain location
        /// </summary>
        /// <returns></returns>
        private static AppDomain GetCurrentDomain()
        {
            return AppDomain.CurrentDomain;
        }

        /// <summary>
        /// Update existing Json with the new data passed as parameter 
        /// </summary>
        /// <param name="request"></param>
        public static void UpdateJson(RatingDetails request)
        {
            try
            {
                var path = $"{GetCurrentDomain().BaseDirectory}//Database.json";

                var requests = GetJsonData();
                requests.Add(request);
                var newJson = JsonConvert.SerializeObject(requests);
                File.WriteAllText(path, newJson);
            }
            catch (Exception)
            {
                //Log Error
            }
        }

        /// <summary>
        /// Function that converts entered name query into what works with Punk API
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string PunkApiBeerNameSearchFormat(string name)
        {
            //To cater for space in the search, I intent to split by space and construct a valid query string based on the documentation 
            var nameArray = name.Split(' ');
            var constructedName = nameArray[0];

            // start looping from 1 as there can be a limit of one item in the nameArray (i.e in case of no spaces ) & that has been taken care of

            for (var i = 1; i < nameArray.Length; i++) constructedName = $"{constructedName}_{nameArray[i]}";

            return constructedName;
        }

        /// <summary>
        /// Convert data to the required response 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<BeerDetails> GenerateGetBeerResponse(string data)
        {
            var apiData = JsonConvert.DeserializeObject<List<BeerDetails>>(data);

            var userRatings = GetJsonData();

            apiData.ForEach(x => x.UserRatings = userRatings);

            return apiData;
        }

        /// <summary>
        /// retrieve Data from Database.json document
        /// </summary>
        /// <returns></returns>
        public static List<RatingDetails> GetJsonData()
        {
            var path = $"{GetCurrentDomain().BaseDirectory}//Database.json";
            using (var streamReader = new StreamReader(path))
            {
                var json = streamReader.ReadToEnd();
                var data = JsonConvert.DeserializeObject<List<RatingDetails>>(json);
                return data;
            }
        }
    }
}