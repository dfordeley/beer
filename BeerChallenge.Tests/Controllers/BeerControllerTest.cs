﻿using System.Net;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BeerChallenge.Controllers;
using BeerChallenge.Helpers;
using BeerChallenge.Models;


namespace BeerChallenge.Tests.Controllers
{
    [TestClass]
    public class BeerControllerTest
    {
        private readonly BeerController _controller;

        public BeerControllerTest()
        {
            _controller = new BeerController();
        }
        
        [TestMethod]
        public void AddRatingInvalidRating()
        {

            // Act
            var result = _controller.AddRating(10,
                new RatingDetails {Comments = "", Rating = 9, Username = "dfordeley@yahoo.co.uk"}).Result;
            
           // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("System.Web.Http.Results.BadRequestErrorMessageResult", result.ToString());
        }

        [TestMethod]
        public void AddRatingInvalidId()
        {

            // Act
            var result =  _controller.AddRating(100000000, new RatingDetails { Comments = "", Rating = 9, Username = "dfordeley@yahoo.co.uk" }).Result;

            var expected = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent("No beer found that matches the ID 100000000")
            };
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("System.Web.Http.Results.BadRequestErrorMessageResult", result.ToString());
        }

        [TestMethod]
        public void PunkApiBeerNameSearchFormatTest()
        {
           // Act
            var result = Utils.PunkApiBeerNameSearchFormat("Oludele Bamidele");

            var expected = "Oludele_Bamidele";
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, result);
        }
    }
}
